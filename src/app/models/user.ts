export class User {

    public id: number;
    public email: string;
    public name: string;
    public firstLastName: string;
    public secondLastName: string;
    public token: string;

    constructor() {
        this.id = 0;
        this.name = "";
    }

}