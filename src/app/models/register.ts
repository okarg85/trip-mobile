export class Register{

    email: string;
    password: string;
    confirmPassword: string;
    phoneNumber: number;

    constructor(){
        this.email = null;
        this.password = null;
        this.confirmPassword = null;
        this.phoneNumber = null;
    }
}