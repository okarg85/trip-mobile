export default {
    urlBase: 'https://raw.githubusercontent.com/rafaelbmblbk/mobileMocks/master/notifications.json',
    localHost: 'http://localhost:3000/api/',
    georgePC: 'http://192.168.100.171:3000/api/',
    rafael: 'http://192.168.1.68:3000/api/'
}