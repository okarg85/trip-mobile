import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageUser } from './storage/user-storage';
import GLOBAL from '../globals';

/*
  Generated class for the AuthHttp provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthHttp {

	urlBase = GLOBAL.georgePC;

	constructor(public http: HttpClient, public storageUser: StorageUser) {
  
	}

	createAuthorizationHeader(): HttpHeaders {
		return new HttpHeaders()
			.set('Accept', 'application/json')
			.set('Content-Type', 'application/json')
			.set('Authorization', this.storageUser.getToken());    
	}

	createUnauthorizationHeader(token?) {
			return new HttpHeaders()
			.set('Accept', 'application/json')
			.set('Content-Type', 'application/json')
	}

	postUnauthorized(url, data) {
		let headers = this.createUnauthorizationHeader();
		return this.http.post(this.urlBase + url, data, { headers: headers });
	}

	get(url) {
		let headers = this.createAuthorizationHeader()
		return this.http.get(this.urlBase + url, { headers: headers });
	}

	post(url, data) {
		let headers = this.createAuthorizationHeader();
		console.log('es post', headers)
		return this.http.post(this.urlBase + url, data, { headers: headers });
	}

	put(url, data) {
		return this.http.put(this.urlBase + url, data, { headers: this.createAuthorizationHeader() });
	}

	delete(url) {
		return this.http.delete(this.urlBase + url, { headers: this.createAuthorizationHeader() });
	}

}