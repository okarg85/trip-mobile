import { Injectable } from '@angular/core';
import { User } from '../../models/user';

@Injectable()
export class StorageUser {


  constructor() { }
  
  setUser(user): void{
    localStorage.setItem('TripNowUser', JSON.stringify(user));
  }

  getUser(): User  {
    let user = new User();
    if(localStorage.getItem('TripNowUser')) {
      user = JSON.parse(localStorage.getItem('TripNowUser'));
    }
    return user;
  }

  getToken(): string {
    let token = "";
    if(localStorage.getItem('TripNowUser')) {
      token = JSON.parse(localStorage.getItem('TripNowUser')).token;
    }
    return token;
  }
  
  deleteUser(): void{
    localStorage.removeItem('TripNowUser');
  }
}
