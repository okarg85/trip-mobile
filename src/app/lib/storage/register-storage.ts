import { Injectable } from '@angular/core';
import { Register } from '../../models/register';

@Injectable()
export class StorageRegister {


  constructor() { }
  
  setRegister(register): void{
    localStorage.setItem('TripNowRegister', JSON.stringify(register));
  }

  getRegister(): Register  {
    let register = new Register();
    if(localStorage.getItem('TripNowRegister')) {
      register = JSON.parse(localStorage.getItem('TripNowRegister'));
    }
    return register;
  }

  getToken(): string {
    let token = "";
    if(localStorage.getItem('TripNowRegister')) {
      token = JSON.parse(localStorage.getItem('TripNowRegister')).token;
    }
    return token;
  }
  
  deleteRegister(): void{
    localStorage.removeItem('TripNowRegister');
  }
}
