import { Injectable } from '@angular/core';
import { AuthHttp } from '../lib/auth-http';
import { SendCode } from '../models/sendCode';
import { CodeVerify } from '../models/codeVerify';


@Injectable({
  providedIn: 'root'
})
export class SMSService {


constructor(private _authHttp: AuthHttp) { }

public sendSMS( data:SendCode ){
  return this._authHttp.postUnauthorized('phone/send-code', data);
}

public verifyNumber( data:CodeVerify ){
  return this._authHttp.postUnauthorized('phone/verify', data);
}

}
