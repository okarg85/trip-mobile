import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthHttp } from '../lib/auth-http';
import { Login } from '../models/login';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token: string = null;

  constructor(
    private _storage: Storage,
    private authHttp: AuthHttp 
  ) { }


  login(login: Login) {
    return this.authHttp.postUnauthorized('login', login);
  }

  getUserInfo() {
    return this.authHttp.get('users/my-account');
  }

}
