import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import  GLOBAL  from '../globals';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {


  private url = GLOBAL.urlBase;

  constructor(public _http: HttpClient) { }
  
  getNotifications(): Observable<any> { 
    return this._http.get(this.url);
    //return this._http.get(this.url + '/login');
  }

  
}
