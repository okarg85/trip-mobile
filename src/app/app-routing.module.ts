import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },


  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },


  { path: 'sign-up', loadChildren: './pages/registry/sign-up/sign-up.module#SignUpPageModule' },
  { path: 'sign-up2', loadChildren: './pages/registry/sign-up2/sign-up2.module#SignUp2PageModule' },
  { path: 'identify', loadChildren: './pages/registry/identify/identify.module#IdentifyPageModule' },
  { path: 'check', loadChildren: './pages/registry/check/check.module#CheckPageModule' },
  { path: 'types', loadChildren: './pages/registry/types/types.module#TypesPageModule' },
  { path: 'vehicle', loadChildren: './pages/registry/vehicle/vehicle.module#VehiclePageModule' },


  { path: 'tabs', loadChildren: './pages/system/tabs/tabs.module#TabsPageModule' },

  { path: 'opcion1', loadChildren: './pages/system/travels/opcion1/opcion1.module#Opcion1PageModule' },

  { path: 'drivermain', loadChildren: './pages/driver/drivermain/drivermain.module#DrivermainPageModule' },
  
  { path: 'confirm', loadChildren: './pages/system/travels/confirm/confirm.module#ConfirmPageModule' },
  { path: 'chats', loadChildren: './pages/system/chats/chats.module#ChatsPageModule' },
  { path: 'trip', loadChildren: './pages/system/trip/trip.module#TripPageModule' },
  { path: 'confirmation-travel', loadChildren: './pages/alerts/confirmation-travel/confirmation-travel.module#ConfirmationTravelPageModule' },
  
 
// { path: 'confirmation-posted', loadChildren: './pages/alerts/confirmation-posted/confirmation-posted.module#ConfirmationPostedPageModule' },
// { path: 'republish', loadChildren: './pages/alerts/republish/republish.module#RepublishPageModule' },
// { path: 'confirmation', loadChildren: './pages/alerts/confirmation/confirmation.module#ConfirmationPageModule' },
// { path: 'talks', loadChildren: './pages/system/talks/talks.module#TalksPageModule' },
// { path: 'settings', loadChildren: './pages/system/settings/settings.module#SettingsPageModule' },
// { path: 'notifications', loadChildren: './pages/system/notifications/notifications.module#NotificationsPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],   
  exports: [RouterModule]
})
export class AppRoutingModule { }
