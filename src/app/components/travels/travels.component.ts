import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { TalksPage } from '../../pages/system/talks/talks.page';

import { CallNumber } from '@ionic-native/call-number/ngx';


@Component({
  selector: 'app-travels',
  templateUrl: './travels.component.html',
  styleUrls: ['./travels.component.scss']
})
export class TravelsComponent implements OnInit {

  private show:boolean;
  @Input() travels;

  constructor(private navCtrl: NavController, 
              private callNumber: CallNumber,
              private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  hide(){
    //this.show = !this.show;
    this.show = !this.show;
    
  }

  details(){
    this.navCtrl.navigateForward('/tabs/details');
  }

  async talk(){
    
    const modal = await this.modalCtrl.create({
      component: TalksPage
    });

    await modal.present();
  }

  call(){ 
    this.callNumber.callNumber("18001010101", false)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}
