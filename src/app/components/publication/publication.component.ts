import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ConfirmationPostedPage } from '../../pages/alerts/confirmation-posted/confirmation-posted.page';

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent implements OnInit {


  constructor(private popoverController: PopoverController) { }

  ngOnInit() {
  }

  async confirmation() {
    const popover = await this.popoverController.create({
      component: ConfirmationPostedPage,
      cssClass:'popover_class_confirmation',
      translucent: false,
      mode: 'ios',
      backdropDismiss: false
    });
     await popover.present();
    
  }


}
