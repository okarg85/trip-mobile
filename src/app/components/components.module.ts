import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TravelsComponent } from './travels/travels.component';
import { TravelComponent } from './travel/travel.component';
import { IonicModule } from '@ionic/angular';
import { PublicationComponent } from './publication/publication.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { TalksPage } from '../pages/system/talks/talks.page';
import { TalksPageModule } from '../pages/system/talks/talks.module';
import { ConfirmationPostedPage } from '../pages/alerts/confirmation-posted/confirmation-posted.page';
import { ConfirmationPostedPageModule } from '../pages/alerts/confirmation-posted/confirmation-posted.module';

import { Keyboard } from '@ionic-native/keyboard/ngx';


@NgModule({
  declarations: [
    TravelsComponent, 
    TravelComponent, 
    PublicationComponent, 
    FavoritesComponent
  ],
  entryComponents: [
    TalksPage,
    ConfirmationPostedPage
  ],
  exports:[
    TravelsComponent,
    TravelComponent,
    PublicationComponent, 
    FavoritesComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    TalksPageModule,
    ConfirmationPostedPageModule
  ],
  providers: [
    Keyboard
  ],
})
export class ComponentsModule { }
