import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.scss']
})
export class TravelComponent implements OnInit {

  constructor( private navCtrl: NavController) { }

  ngOnInit() {
  }

  detailNotification(){
    this.navCtrl.navigateForward('/tabs/request');
  }

  detailTravel(){
    this.navCtrl.navigateForward('/tabs/info-travel');
  }
}
