import { Component, OnInit } from '@angular/core';

import {  ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

declare var mapboxgl: any;
@Component({
  selector: 'app-points',
  templateUrl: './points.page.html',
  styleUrls: ['./points.page.scss'],
})
export class PointsPage implements OnInit{

  lat:number;
  lng:number;
  
  constructor(private modalCtrl: ModalController, private navCtrl: NavController) { 
    this.lat = 19.293558;
    this.lng = -99.654000;
  }

  ngOnInit() {
    
//   
//    var map = new mapboxgl.Map({
//      style: 'mapbox://styles/mapbox/light-v10',
//      center: [ -99.655504, 19.289658],
//      zoom: 15.5,
//      pitch: 45,
//      bearing: -17.6,
//      container: 'map'
//    });
//mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWVsYmwiLCJhIjoiY2p0dWR4aGU0MGRsdDRkcXlvMm5pcnlrMSJ9.ykj0zrAzLfYBs1x8ARFhkg';
//var map = new mapboxgl.Map({
//  container: 'map',
//  style: 'mapbox://styles/mapbox/streets-v11',
//  center: [-99.655504, 19.289658],
//  zoom: 14
//  });
//  
//  map.on('load', function(){
//    map.resize();   
//      //MARKER
//      var marker = new mapboxgl.Marker()
//      .setLngLat([ -99.655504, 19.289658])
//      .addTo(map);
//  });
//  
}

  search(){
    this.navCtrl.navigateForward('/tabs/search');
  }

  save(){
    this.navCtrl.navigateForward('/tabs/travel');
  }
}
