import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelPostPage } from './travel-post.page';

describe('TravelPostPage', () => {
  let component: TravelPostPage;
  let fixture: ComponentFixture<TravelPostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelPostPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelPostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
