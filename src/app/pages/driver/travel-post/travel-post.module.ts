import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TravelPostPage } from './travel-post.page';
import { ConfirmationPage } from '../../alerts/confirmation/confirmation.page';
import { ConfirmationPageModule } from '../../alerts/confirmation/confirmation.module';

const routes: Routes = [
  {
    path: '',
    component: TravelPostPage
  }
];

@NgModule({
  entryComponents: [
    ConfirmationPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmationPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TravelPostPage]
})
export class TravelPostPageModule {}
