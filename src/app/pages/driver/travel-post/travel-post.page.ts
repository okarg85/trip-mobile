import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {  ModalController } from '@ionic/angular';
import { ConfirmationPage } from '../../alerts/confirmation/confirmation.page';
import { GenericAlert } from '../../../lib/generic-alerts';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-travel-post',
  templateUrl: './travel-post.page.html',
  styleUrls: ['./travel-post.page.scss'],
})
export class TravelPostPage implements OnInit {

  constructor(private modalCtrl: ModalController, 
              private navCtrl: NavController,
              private genericAlert: GenericAlert,
              public popoverController: PopoverController) { }

  ngOnInit() {
  }

  points(){
    this.navCtrl.navigateForward('/tabs/points');
  }

  search(){
    this.navCtrl.navigateForward('/tabs/search');
  }

  async confirmationTavel() {
    const popover = await this.popoverController.create({
      component: ConfirmationPage,
      cssClass:'popover_class',
      translucent: false,
      mode: 'ios',
      backdropDismiss: false
    });
     await popover.present();
    setTimeout( ()=>this.popoverController.dismiss(), 1 * 1000);
  }
//  async confirmationTavel() {
//    const modal = await this.modalCtrl.create({
//      component: ConfirmationPage
//    });
//
//    await modal.present();
//  }
//
//confirmationTavel(){
//  this.genericAlert.presentAlertPrompt();
//}


}
