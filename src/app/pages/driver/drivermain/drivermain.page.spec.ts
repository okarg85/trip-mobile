import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivermainPage } from './drivermain.page';

describe('DrivermainPage', () => {
  let component: DrivermainPage;
  let fixture: ComponentFixture<DrivermainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrivermainPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivermainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
