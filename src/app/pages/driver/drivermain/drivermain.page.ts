import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-drivermain',
  templateUrl: './drivermain.page.html',
  styleUrls: ['./drivermain.page.scss'],
})
export class DrivermainPage implements OnInit {

  @ViewChild(IonSegment) segment :IonSegment;

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
    this.segment.value = 'Viajes';
  }
 
  travelPost(){
    this.navCtrl.navigateForward('/travel');
  }

  infoTravel(){
    this.navCtrl.navigateForward('/info-travel');
  }

}
