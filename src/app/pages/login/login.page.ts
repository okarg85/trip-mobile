import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { StorageUser } from '../../lib/storage/user-storage';
import { UserService } from '../../services/user.service';
import { GenericAlert } from '../../lib/generic-alerts';
import { Login } from '../../models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  public login: Login;

  constructor(
    private navCtrl: NavController,
    private userService: UserService,
    private alertService: GenericAlert,
    private storageUser: StorageUser
    ) {
      this.login = new Login();
    }


  ngOnInit() {
  }

  submit() {
    this.navCtrl.navigateRoot('/tabs', { animated:true} );
    /*
    this.userService.login(this.login)
    .subscribe((data: any) => {
      let user = data.user;
      this.storageUser.setUser(user);
      this.navCtrl.navigateRoot('/tabs');
    }, error => {
      console.log(error);
    });
    */
  }


  signUp() {
    this.navCtrl.navigateForward('/check');
  }

}
