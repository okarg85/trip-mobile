import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.page.html',
  styleUrls: ['./vehicle.page.scss'],
})
export class VehiclePage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  main(){
    this.navCtrl.navigateRoot('/tabs');
  }
}
