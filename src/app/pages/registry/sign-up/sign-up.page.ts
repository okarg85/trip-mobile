import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SendCode } from '../../../models/sendCode';


import { SMSService } from '../../../services/sms.service';



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  public sendCode: SendCode;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private navCtrl: NavController,
    private smsService: SMSService
  ) {
    this.sendCode = new SendCode();
   }

  ngOnInit() {
  }

  submit(){
    this.navCtrl.navigateForward('/check');
    
    this.smsService.sendSMS(this.sendCode)
    .subscribe(data => {
      console.log(data);
      this.navCtrl.navigateForward('/check');
      
    }, error => {
      console.log(error);
    });
  
  }
}
