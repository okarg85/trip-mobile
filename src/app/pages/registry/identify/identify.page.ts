import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-identify',
  templateUrl: './identify.page.html',
  styleUrls: ['./identify.page.scss'],
})
export class IdentifyPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  submit(){
    this.navCtrl.navigateRoot('/tabs');
  }
}
