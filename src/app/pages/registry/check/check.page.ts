import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import { SMSService } from '../../../services/sms.service';

import { CodeVerify } from '../../../models/codeVerify';

@Component({
  selector: 'app-check',
  templateUrl: './check.page.html',
  styleUrls: ['./check.page.scss'],
})
export class CheckPage implements OnInit {

  public codeVerify: CodeVerify;
  public numberCode1: number;
  public numberCode2: number;  
  public numberCode3: number;  
  public numberCode4: number;

  constructor(private navCtrl: NavController,
              private sMSService :SMSService) {
    this.codeVerify = new CodeVerify();
   }

  ngOnInit() {
  }

  submit(){
    this.navCtrl.navigateForward('/sign-up2');
    let code = this.numberCode1 + this.numberCode2 + this.numberCode3 + this.numberCode4;
    this.codeVerify.code = code;
    this.codeVerify.phoneNumber = 7225929528;
    console.log(this.codeVerify);

    this.sMSService.verifyNumber(this.codeVerify)
    .subscribe(data => {
      console.log(data);
      
    }, error => {
      console.log(error);
    });
  }
}
