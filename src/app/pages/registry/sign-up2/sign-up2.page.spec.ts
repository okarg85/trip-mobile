import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUp2Page} from './sign-up2.page';

describe('SignUp2Page', () => {
  let component: SignUp2Page;
  let fixture: ComponentFixture<SignUp2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUp2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUp2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});