import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-types',
  templateUrl: './types.page.html',
  styleUrls: ['./types.page.scss'],
})
export class TypesPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  vehicle(){
    this.navCtrl.navigateForward('/vehicle');
  }
}
