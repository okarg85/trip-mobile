import { Component, OnInit } from '@angular/core';

import { ConfirmationPage } from '../../alerts/confirmation/confirmation.page';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-confirmation-posted',
  templateUrl: './confirmation-posted.page.html',
  styleUrls: ['./confirmation-posted.page.scss'],
})
export class ConfirmationPostedPage implements OnInit {

  constructor(
              private popoverController: PopoverController) { }

  ngOnInit() {
  }


  async confirmationPost() {
    this.popoverController.dismiss();
    const popover = await this.popoverController.create({
      component: ConfirmationPage,
      cssClass:'popover_class',
      translucent: false,
      mode: 'ios',
      backdropDismiss: false
    });
     await popover.present();
    setTimeout( ()=>this.popoverController.dismiss(), 1 * 1000);
  }

}
