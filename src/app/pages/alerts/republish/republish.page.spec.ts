import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepublishPage } from './republish.page';

describe('RepublishPage', () => {
  let component: RepublishPage;
  let fixture: ComponentFixture<RepublishPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepublishPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepublishPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
