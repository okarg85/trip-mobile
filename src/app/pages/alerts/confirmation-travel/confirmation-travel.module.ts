import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmationTravelPage } from './confirmation-travel.page';
import { ConfirmationPage } from '../../alerts/confirmation/confirmation.page';
import { ConfirmationPageModule } from '../../alerts/confirmation/confirmation.module';

@NgModule({
  entryComponents: [
    ConfirmationPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmationPageModule,
  ],
  declarations: [ConfirmationTravelPage]
})
export class ConfirmationTravelPageModule {}
