import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ConfirmationPage } from '../../alerts/confirmation/confirmation.page';


@Component({
  selector: 'app-confirmation-travel',
  templateUrl: './confirmation-travel.page.html',
  styleUrls: ['./confirmation-travel.page.scss'],
})
export class ConfirmationTravelPage implements OnInit {

ocultar='';

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {
  }

  async confirmationPost() {
    this.popoverController.dismiss();
    const popover = await this.popoverController.create({
      component: ConfirmationPage,
      cssClass:'popover_class',
      translucent: false,
      mode: 'ios',
      backdropDismiss: false
    });

     this.ocultar='animated fadeOut fast';
     
     await popover.present();
    setTimeout( ()=>this.popoverController.dismiss(), 1 * 800);
  }

}
