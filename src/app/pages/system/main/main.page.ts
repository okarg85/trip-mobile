import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment, ModalController, IonSlides } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  public travels: any;
  @ViewChild(IonSegment) segment: IonSegment;
  @ViewChild('slidePrincipal') slides: IonSlides;

  slideOpts = {

    slidesPerView: 1,
    freeMode: true,
    ionSlideNextStart: true
  }

  constructor(private modalCtrl: ModalController, public http:HttpClient, private navCtrl: NavController) { 
    this.loadData();
  }

  ngOnInit() {
    this.segment.value = 'todos';
    this.slides.lockSwipes( true );
  }

  search() {
  this.navCtrl.navigateForward('/tabs/search');
 }



  travelPost() {
    this.navCtrl.navigateForward('/tabs/travel');
  }

  loadData() {
    let data: Observable<any>;
    data = this.http.get('https://raw.githubusercontent.com/rafaelbmblbk/mobileMocks/master/travels.json');
    data.subscribe(result => {
      this.travels = result;
    });
  }

  cambiarOpcion( event ) {
   if (  event.detail.value === 'todos') {
     this.slides.lockSwipes( false );
    this.loadData();
    this.slides.slideTo(0);
    this.slides.lockSwipes( true );
   }

   if (  event.detail.value === 'Viajes') {
     this.slides.lockSwipes( false );
    this.slides.slideTo(1);
    this.slides.lockSwipes( true );
   }

   if (  event.detail.value === 'publicados') {
     this.slides.lockSwipes( false );
    this.slides.slideTo(2);
    this.slides.lockSwipes( true );
   }

   if (  event.detail.value === 'Favorito') {
     this.slides.lockSwipes( false );
    this.slides.slideTo(3);
    this.slides.lockSwipes( true );
   }
  }
}
