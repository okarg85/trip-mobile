import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoldalSearchPage } from './moldal-search.page';

describe('MoldalSearchPage', () => {
  let component: MoldalSearchPage;
  let fixture: ComponentFixture<MoldalSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoldalSearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoldalSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
