import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: User;

  constructor(
    private userService: UserService
  ) {
    this.user = new User();
  }

  ngOnInit() {
    this.fetchUserInfo();
  }

  fetchUserInfo() {
    this.userService.getUserInfo()
    .subscribe((data: any) => {
      this.user = data.user;
    }, error => {
      console.log(error);
    })
  }

}
