import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-opcion1',
  templateUrl: './opcion1.page.html',
  styleUrls: ['./opcion1.page.scss'],
})
export class Opcion1Page implements OnInit {

  public travels:any;
  constructor(public http:HttpClient) { 
    this.loadData();
    
  }

  ngOnInit() {

  
  }

  loadData(){
    let data:Observable<any>;
    data = this.http.get('https://raw.githubusercontent.com/rafaelbmblbk/mobileMocks/master/travels.json');
    data.subscribe(result =>{
      this.travels = result;
    });
  }

}
