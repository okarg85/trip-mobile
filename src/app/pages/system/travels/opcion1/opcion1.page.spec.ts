import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Opcion1Page } from './opcion1.page';

describe('Opcion1Page', () => {
  let component: Opcion1Page;
  let fixture: ComponentFixture<Opcion1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Opcion1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Opcion1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
