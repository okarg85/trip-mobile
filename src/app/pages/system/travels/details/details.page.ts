import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { ConfirmationTravelPage } from '../../../alerts/confirmation-travel/confirmation-travel.page'
import { TalksPage } from '../../talks/talks.page';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  lat:number;
  lng:number;

  lati:number;
  lngi:number;

  constructor(private modalCtrl : ModalController,
              private popoverController: PopoverController, 
              private navCtrl: NavController,
              private socialSharing: SocialSharing) { 

                this.lat = 19.293558;
                this.lng = -99.654000;

                this.lati = 19.292282; 
                this.lngi = -99.655009;
  }

  ngOnInit() {
  }


  async confimarTravel() {
    const popover = await this.popoverController.create({
      component: ConfirmationTravelPage,
      cssClass:'popover_class_confirmation',
      translucent: false,
      mode: 'ios',
      backdropDismiss: false
    });
     await popover.present();
  }

  async openTalk(){
    
    const modal = await this.modalCtrl.create({
      component: TalksPage
    });

    await modal.present();
  }

  shareTravel(){
    this.socialSharing.share(
      'Mira este Viaje en Trip Now',
      'Comparte',
      '',
      'https://www.google.com',
    );
  }

}

