import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { DetailsPage } from './details.page';

import { Routes, RouterModule } from '@angular/router';

import { ConfirmationTravelPage } from '../../../alerts/confirmation-travel/confirmation-travel.page';
import { ConfirmationTravelPageModule } from '../../../alerts/confirmation-travel/confirmation-travel.module';
import { TalksPage } from '../../talks/talks.page';
import { TalksPageModule } from '../../talks/talks.module';

import { AgmCoreModule } from '@agm/core';
 
const routes: Routes = [
  {
    path: '',
    component: DetailsPage
  }
];


@NgModule({
  entryComponents: [
    ConfirmationTravelPage,
    TalksPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmationTravelPageModule,
    TalksPageModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAt2fPR6vw50ZahuqjLwZrh4aQwY_DF-Bo'}),
    RouterModule.forChild(routes)
  ],
  declarations: [DetailsPage]
})
export class DetailsPageModule {}
