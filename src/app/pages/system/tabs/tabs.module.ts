import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';


const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:[
      {
        path:'main',
        children:[
          {
            path:'',
            loadChildren:'../main/main.module#MainPageModule'
          },
          { 
            path: 'search', 
            loadChildren: '../moldal-search/moldal-search.module#MoldalSearchPageModule' 
          },
          { path: 'travel', 
            loadChildren: '../../driver/travel-post/travel-post.module#TravelPostPageModule' 
          },
          {
            path: 'points', 
            loadChildren: '../../driver/points/points.module#PointsPageModule' 
          },
          { 
            path: 'details', 
            loadChildren: '../travels/details/details.module#DetailsPageModule'
          },
          { 
            path: 'request', 
            loadChildren: '../../driver/request/request.module#RequestPageModule' 
          },
          { 
            path: 'info-travel', 
            loadChildren: '../../driver/info-travel/info-travel.module#InfoTravelPageModule' 
          },
        ]
        
      },
      {
        path:'chats',
        loadChildren:'../chats/chats.module#ChatsPageModule'
      },
      {
        path:'notifications',
        children: [
          {
            path: '',
            loadChildren:'../notifications/notifications.module#NotificationsPageModule'
          },
          {
            path: 'request', 
            loadChildren: '../../driver/request/request.module#RequestPageModule'
          }

        ],
      },
      {
        path:'settings',
        children:[
          {
            path:'',
            loadChildren:'../settings/settings.module#SettingsPageModule',
          },
          { 
            path: 'info', 
            loadChildren: '../info-vehicle/info-vehicle.module#InfoVehiclePageModule' 
          },
          { 
            path: 'profile', 
            loadChildren: '../profile/profile.module#ProfilePageModule' 
          },
          { 
            path: 'help', 
            loadChildren: '../help/help.module#HelpPageModule' 
          },
          
        ]
      },
    ]
  },
  {    
    path:'',
    redirectTo:'tabs/main',
    pathMatch:'tabs/main'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
