import { Component, OnInit } from '@angular/core';
import { GenericAlert } from '../../../lib/generic-alerts';

@Component({
  selector: 'app-info-vehicle',
  templateUrl: './info-vehicle.page.html',
  styleUrls: ['./info-vehicle.page.scss'],
})
export class InfoVehiclePage implements OnInit {

  constructor(private alertService: GenericAlert) { }

  ngOnInit() {
  }

  AlertaModificar(){
    this.alertService.
    editVehicleAlert('Para Poder actualizar la información de tu de tu automóvil necesitas ponerte en contacto con los administradores de Trip Now');
  }

}
