import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoVehiclePage } from './info-vehicle.page';

describe('InfoVehiclePage', () => {
  let component: InfoVehiclePage;
  let fixture: ComponentFixture<InfoVehiclePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoVehiclePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoVehiclePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
