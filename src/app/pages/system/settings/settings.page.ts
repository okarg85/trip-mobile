import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { GenericAlert } from '../../../lib/generic-alerts';
import { AlertController } from '@ionic/angular';
import { StorageUser } from '../../../lib/storage/user-storage';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  

  constructor(
    private navCtrl: NavController,
    private alertService: GenericAlert,
    private alertController: AlertController,
    private storageUser: StorageUser
  ) { }

  ngOnInit() {
    
  }


  help(){
    this.navCtrl.navigateForward('/tabs/tabs/settings/help');
  }

  info(){
    this.navCtrl.navigateForward('/tabs/tabs/settings/info');
  }


  profile(){
    this.navCtrl.navigateForward('/tabs/tabs/settings/profile');
  }

 

  async close(){
    const alert = await this.alertController.create({
      header: 'Cerrar sesión',
      message: '¿Seguro que deaea cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.storageUser.deleteUser();
            this.navCtrl.navigateRoot('/home');
          }
        }
      ]
    });

    await alert.present();
  }
  
}
