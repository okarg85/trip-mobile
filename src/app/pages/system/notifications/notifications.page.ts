import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../../services/notifications.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  public notification:any;
  constructor(public _notificationsService: NotificationsService, private navCtrl: NavController) { 
    this.loadData();
  }

  ngOnInit() {
  }

  loadData() {
    this._notificationsService.getNotifications()
    .subscribe(data => {
      this.notification = data;
    }, error => {

    });
  }

  detailNotification(){
    this.navCtrl.navigateForward('/tabs/tabs/notifications/request');
  }

}
