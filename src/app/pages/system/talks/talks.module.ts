import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AutosizeModule } from 'ngx-autosize';

import { IonicModule } from '@ionic/angular';

import { TalksPage } from './talks.page';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutosizeModule
  ],
  declarations: [TalksPage]
})
export class TalksPageModule {
  
constructor(private keyboard: Keyboard) {
  
this.keyboard.show();

this.keyboard.hide();
 }
}
