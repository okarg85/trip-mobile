import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalksPage } from './talks.page';

describe('TalksPage', () => {
  let component: TalksPage;
  let fixture: ComponentFixture<TalksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalksPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
