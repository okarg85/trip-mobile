import { Component, OnInit,ViewChild } from '@angular/core';
import { ModalController,IonContent } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-talks',
  templateUrl: './talks.page.html',
  styleUrls: ['./talks.page.scss'],
})
export class TalksPage implements OnInit {
  messages = [
    {
      user: 'Paola',
      createdAt: 1554090856000,
      msg: 'Hola'
    }
  ]
  mensaje = [
    {
      user: '',
      createdAt: 1554090856000,
      msg: ''
    }
  ]
  public mensajes=[];
  public msg : string;
  public newMsg:string;

  @ViewChild(IonContent) content: IonContent;
  constructor(private modalCtrl: ModalController) { }

     
  ngOnInit() {
  }

  salir(){
    this.modalCtrl.dismiss();
  }

  sendMessage(){
    this.messages.push({
      user: '',
      createdAt: new Date().getTime(),
      msg: this.newMsg
    });
    
    this.newMsg = '';
   
    setTimeout(() => {
      this.content.scrollToBottom(200);
    });
  }

sendMensaje(){
       
    this.mensajes.push({
    user: 'Oscar',
    createdAt: new Date().getTime(),
    msg: this.msg
});
  
  this.msg = "";
 

      setTimeout(() => {
        this.content.scrollToBottom(200);
      });
     // return this.sendMessage();
  }
 }
