import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TalksPage } from '../talks/talks.page';
@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async talk(){
    
    const modal = await this.modalCtrl.create({
      component: TalksPage,
      componentProps: {
        nombre:'Rafa',
        pais:'mex'
      }
      
    });

    await modal.present();
  }

}
