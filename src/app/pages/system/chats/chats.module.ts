import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChatsPage } from './chats.page';

import { TalksPage } from '../talks/talks.page';
import { TalksPageModule } from '../talks/talks.module';
//import { Keyboard } from '@ionic-native/keyboard/ngx';

const routes: Routes = [
  {
    path: '',
    component: ChatsPage
  }
];

@NgModule({
  entryComponents: [
    TalksPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TalksPageModule,
    //Keyboard,
    RouterModule.forChild(routes)
  ],
  declarations: [ChatsPage]
})
export class ChatsPageModule {
  //constructor(private keyboard: Keyboard) {
   // this.keyboard.show();

   // this.keyboard.hide();
   //}
}
