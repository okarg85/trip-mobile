import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageUser } from '../../lib/storage/user-storage';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage {

  constructor(
    private navCtrl: NavController,
    private storageUser: StorageUser
  ) {

  }

  ionViewWillEnter() {
    let user = this.storageUser.getUser();
    console.log( user , !!user );
  }

  login() {                                      /*función para navegar al loogin*/
    this.navCtrl.navigateRoot('/login');        /*navigateRoot bloquea el Back */
  }

  signUp() {                                   /*función para navegar al registro */
    this.navCtrl.navigateForward('/sign-up'); /*navigateForward permite back*/
  }

}

